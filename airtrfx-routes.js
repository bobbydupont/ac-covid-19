const Excel = require("exceljs");
const minimist = require("minimist");
const fs = require("fs");
var dir = './airTRFX/tmp';

const {
	sheetsToParse,
} = require("./app/config/app.json");

/* RTT = Ready to Takeoff campiagn */
let rttRoutes = [];
let rttRoutesFR = [];
let rttRoutesES = [];
let rtt = {};
let rttFR = {};
let rttES = {};

const args = minimist(process.argv.slice(2));

console.log("sheetsToParse = ", sheetsToParse);

try {
	if (!args.f) {
		throw new Error("Missing File argument");
	}
} catch (err) {
	console.log("err = ", err);
}

const workbook = new Excel.Workbook();
console.log("sheetsToParse = ", sheetsToParse);
const parseDoc = async () => {
	console.log("args = ", args.f);
	const worksheets = await workbook.xlsx.readFile(args.f);
	worksheets.eachSheet((worksheet) => {
		worksheet.eachRow({
			includeEmpty: false,
		}, (row, rowNumber) => {
			if (rowNumber >= 3) {
				//console.log('Row ${rowNumber} = ${JSON.stringify(row.values)}');
				rtt = { "url" : String(row.getCell(1).text), "title" : String(row.getCell(2).text), "description" : String(row.getCell(3).text) };
				rttFR = { "url" : String(row.getCell(4).text), "title" : String(row.getCell(5).text), "description" : String(row.getCell(6).text) };
				rttES = { "url" : String(row.getCell(7).text), "title" : String(row.getCell(8).text), "description" : String(row.getCell(9).text) };
				if (Object.entries(rtt.url).length !== 0){
					rttRoutes.push(rtt);
				}
				if (Object.entries(rttFR.url).length !== 0){
					rttRoutesFR.push(rttFR);
				}
				if (Object.entries(rttES.url).length !== 0){
					rttRoutesES.push(rttES);
				}
			}
		});
	});
	var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    d = [year, month, day].join('-');
    dir = './airTRFX/'+d;
    if (!fs.existsSync(dir)){
    	fs.mkdirSync(dir);
	}
	//var nameToWrite = worksheet.name.trim().toLowerCase().replace(/\s+/g, '-').replace(/-{2,}/g,'-');
	fs.writeFile('./airTRFX/' + d + '/targetedContent-en.json', /*travelTable*/ JSON.stringify(rttRoutes), (err) => {
		if (err) throw err;
		console.log('targetedContent table saved.');
	});
	fs.writeFile('./airTRFX/' + d + '/targetedContent-fr.json', /*travelTableFR*/ JSON.stringify(rttRoutesFR), (err) => {
		if (err) throw err;
		console.log('FRENCH targetedContent table saved.');
	});
	fs.writeFile('./airTRFX/' + d + '/targetedContent-es.json', /*travelTableES*/ JSON.stringify(rttRoutesES), (err) => {
		if (err) throw err;
		console.log('SPANISH targetedContent table saved.');
	});
	rttRoutes = [];
	rttRoutesFR = [];
	rttRoutesES = [];
};

parseDoc();
