const Excel = require("exceljs");
const minimist = require("minimist");
const fs = require("fs");

const {
	sheetsToParse,
} = require("./app/config/app.json");

const args = minimist(process.argv.slice(2));

console.log("sheetsToParse = ", sheetsToParse);
let travelTable;
let travelTableFR;
try {
	if (!args.f) {
		throw new Error("Missing File argument");
	}
} catch (err) {
	console.log("err = ", err);
}

const workbook = new Excel.Workbook();
console.log("sheetsToParse = ", sheetsToParse);
const parseDoc = async () => {
	console.log("args = ", args.f);
	const worksheets = await workbook.xlsx.readFile(args.f);
	worksheets.eachSheet((worksheet) => {
		console.log(worksheet.name);
		if (String(worksheet.name) != "COPY" && String(worksheet.name) != "SPECIAL FLIGHTS") {
			travelTable += '<table class="ac-table ac-table-columns ac-table-rows ac-table-full-width" cellspacing="0" cellpadding="0">';
			//travelTableFR += '<table class="ac-table ac-table-columns ac-table-rows ac-table-full-width" cellspacing="0" cellpadding="0">';
			travelTable += "<thead>";
			//travelTableFR += "<thead>";
			if(worksheet.name == "MAY-JUL - DOM" || worksheet.name == "MAY-JUL - USA" || worksheet.name == "MAY-JUL - CARIB-MEX" || worksheet.name == "MAY-JUL - ATLANTIC" || worksheet.name == "MAY-JUL - PACIFIC" || worksheet.name == "MAY-JUL - South AM"){
				travelTable += `<tr><th scope="row" data-label="${String(worksheet.getCell("A1").text)}">${String(worksheet.getCell("A1").text)}</th><th data-label="${String(worksheet.getCell("B1").text)}">${String(worksheet.getCell("B1").text)}</th><th data-label="${String(worksheet.getCell("C1").text)}">${String(worksheet.getCell("C1").text)}</th><th data-label="${String(worksheet.getCell("D1").text)}">${String(worksheet.getCell("D1").text)}</th><th data-label="${String(worksheet.getCell("E1").text)}">${String(worksheet.getCell("E1").text)}</th></tr>`;
				travelTableFR += `<tr><th scope="row" data-label="${String(worksheet.getCell("G1").text)}">${String(worksheet.getCell("G1").text)}</th><th data-label="${String(worksheet.getCell("H1").text)}">${String(worksheet.getCell("H1").text)}</th><th data-label="${String(worksheet.getCell("I1").text)}">${String(worksheet.getCell("I1").text)}</th><th data-label="${String(worksheet.getCell("J1").text)}">${String(worksheet.getCell("J1").text)}</th><th data-label="${String(worksheet.getCell("K1").text)}">${String(worksheet.getCell("K1").text)}</th></tr>`;
				travelTable += "</thead>";
				travelTableFR += "</thead>";
				travelTable += "<tbody>";
				travelTableFR += "<tbody>";
				worksheet.eachRow({
					includeEmpty: false,
				}, (row, rowNumber) => {
					if (String(row.getCell(1).value) != worksheet.getCell("A1").value && String(row.getCell(2).value) != worksheet.getCell("B1").value) {
						console.log(`Row ${rowNumber} = ${JSON.stringify(row.values)}`);
						travelTable += `<tr><th scope="row" data-label="${String(worksheet.getCell("A1").text)}">${String(row.getCell(1).text)}</th><td data-label="${String(worksheet.getCell("B1").text)}">${String(row.getCell(2).text)}</td><td data-label="${String(worksheet.getCell("C1").text)}">${String(row.getCell(3).text)}</td><td data-label="${String(worksheet.getCell("D1").text)}">${String(row.getCell(4).text)}</td><td data-label="${String(worksheet.getCell("E1").text)}">${String(row.getCell(5).text)}</td></tr>`;
						travelTableFR += `<tr><th scope="row" data-label="${String(worksheet.getCell("G1").text)}">${String(row.getCell(7).text)}</th><td data-label="${String(worksheet.getCell("H1").text)}">${String(row.getCell(8).text)}</td><td data-label="${String(worksheet.getCell("I1").text)}">${String(row.getCell(9).text)}</td><td data-label="${String(worksheet.getCell("J1").text)}">${String(row.getCell(10).text)}</td><td data-label="${String(worksheet.getCell("K1").text)}">${String(row.getCell(11).text)}</td></tr>`;
					}
				});
			}else {
				travelTable += `<tr><th scope="row">${String(worksheet.getCell("A1").text)}</th><th>${String(worksheet.getCell("B1").text)}</th></tr>`;
				//travelTableFR += `<tr><th scope="row" data-label="+${String(worksheet.getCell("D1").text)}+">${String(worksheet.getCell("D1").text)}</th><th data-label="${String(worksheet.getCell("E1").text)}">${String(worksheet.getCell("E1").text)}</th></tr>`;
				travelTable += "</thead>";
				//travelTableFR += "</thead>";
				travelTable += "<tbody>";
				//travelTableFR += "<tbody>";
				worksheet.eachRow({
					includeEmpty: false,
				}, (row, rowNumber) => {
					if (String(row.getCell(1).value) != worksheet.getCell("A1").value && String(row.getCell(2).value) != worksheet.getCell("B1").value) {
						console.log(`Row ${rowNumber} = ${JSON.stringify(row.values)}`);
						travelTable += `<tr><th scope="row">${String(row.getCell(1).text)}</th><td>${String(row.getCell(2).text)}</td></tr>`;
						//travelTableFR += `<tr><th scope="row" data-label="${String(worksheet.getCell("D1").text)}">${String(row.getCell(4).text)}</th><td data-label="${String(worksheet.getCell("E1").text)}">${String(row.getCell(5).text)}</td></tr>`;
					}
				});
			}
			travelTable += "</tbody></table>";
			//travelTableFR += "</tbody></table>";
			fs.writeFile(`./tables/${worksheet.name.trim()}.html`, travelTable, (err) => {
				if (err) throw err;
				console.log(`${worksheet.name} table saved.`);
			});
			/*
			fs.writeFile(`./tables/fr/${worksheet.name.trim()}.html`, travelTableFR, (err) => {
				if (err) throw err;
				console.log(`FRENCH${worksheet.name} table saved.`);
			});
			*/
			travelTable = "";
			travelTableFR = "";
		}
		console.log(travelTable);
	});
};

parseDoc();
