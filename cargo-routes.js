const Excel = require("exceljs");
const minimist = require("minimist");
const fs = require("fs");

const {
	sheetsToParse,
} = require("./app/config/app.json");

const args = minimist(process.argv.slice(2));

console.log("sheetsToParse = ", sheetsToParse);
let travelTable;
let travelTableFR;
try {
	if (!args.f) {
		throw new Error("Missing File argument");
	}
} catch (err) {
	console.log("err = ", err);
}

const workbook = new Excel.Workbook();
console.log("sheetsToParse = ", sheetsToParse);
const parseDoc = async () => {
	console.log("args = ", args.f);
	const worksheets = await workbook.xlsx.readFile(args.f);
	worksheets.eachSheet((worksheet) => {
		console.log(worksheet.name);
		if (String(worksheet.name) == "widebody schedule EN") {
			travelTable += '<table id="sort-table" class="table table-element">';
			travelTable += "<thead>";
            travelTable += `<tr><th data-sort="string" class="sortable">${String(worksheet.getCell("A1").text)}</th><th data-sort="string" class="sortable">${String(worksheet.getCell("B1").text)}</th><th data-sort="string" class="sortable">${String(worksheet.getCell("C1").text)}</th><th data-sort="string" class="sortable">${String(worksheet.getCell("D1").text)}</th></tr>`;
            travelTable += "</thead>";
            travelTable += "<tbody>";
            worksheet.eachRow({
                includeEmpty: false,
            }, (row, rowNumber) => {
                if (String(row.getCell(1).value) != worksheet.getCell("A1").value && String(row.getCell(2).value) != worksheet.getCell("B1").value && String(row.getCell(3).value) != worksheet.getCell("C1").value && String(row.getCell(4).value) != worksheet.getCell("D1").value) {
                    console.log(`Row ${rowNumber} = ${JSON.stringify(row.values)}`);
                    travelTable += `<tr><td class="theCity">${String(row.getCell(1).text)}</td><td class="theCity">${String(row.getCell(2).text)}</td><td>${String(row.getCell(3).text)}</td><td>${String(row.getCell(4).text)}</td></tr>`;
                }
            });
			travelTable += "</tbody></table>";
			fs.writeFile(`./cargo/${worksheet.name.trim()}.html`, travelTable, (err) => {
				if (err) throw err;
				console.log(`${worksheet.name} table saved.`);
			});
			travelTable = "";
		}else if(String(worksheet.name) == "widebody schedule FR"){
            travelTableFR += '<table id="sort-table" class="table table-element">';
            travelTableFR += "<thead>";
            travelTableFR += `<tr><th data-sort="string" class="sortable">${String(worksheet.getCell("A1").text)}</th><th data-sort="string" class="sortable">${String(worksheet.getCell("B1").text)}</th><th data-sort="string" class="sortable">${String(worksheet.getCell("C1").text)}</th><th data-sort="string" class="sortable">${String(worksheet.getCell("D1").text)}</th></tr>`;
            travelTableFR += "</thead>";
            travelTableFR += "<tbody>";
            worksheet.eachRow({
                includeEmpty: false,
            }, (row, rowNumber) => {
                if (String(row.getCell(1).value) != worksheet.getCell("A1").value && String(row.getCell(2).value) != worksheet.getCell("B1").value && String(row.getCell(3).value) != worksheet.getCell("C1").value && String(row.getCell(4).value) != worksheet.getCell("D1").value) {
                    console.log(`Row ${rowNumber} = ${JSON.stringify(row.values)}`);
                    travelTableFR += `<tr><td class="theCity">${String(row.getCell(1).text)}</td><td class="theCity">${String(row.getCell(2).text)}</td><td>${String(row.getCell(3).text)}</td><td>${String(row.getCell(4).text)}</td></tr>`;
                }
            });
            travelTableFR += "</tbody></table>";
            fs.writeFile(`./cargo/fr/${worksheet.name.trim()}.html`, travelTableFR, (err) => {
				if (err) throw err;
				console.log(`FRENCH${worksheet.name} table saved.`);
            });
            travelTableFR = "";
        }
		console.log(travelTable);
	});
};

parseDoc();
