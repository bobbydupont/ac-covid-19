#Run
File path can be relative of full path

```
node routes-suspensions.js - f {{filePath}}
```

node routes-suspensions.js -f "c:\Users\DUPBOB01\ac-covid-19\src\ROUTE SUSPENSIONS AND REVISED SCHEDULE MARCH 24_KCW-10-59pm.xlsx"  
node routes-suspensions.js -f "C:\Users\..\src\route-suspensions.xlsx"  
node routesJSON.js -f "C:\Users\..\src\route-suspensions.xlsx  
node cargo-routes.js -f "C:\Users\..\src\route-suspensions.xlsx  
  
#Known issues  
  
* march.html starts with undefined then the rest of the table
* Still need to manually add line breaks 

#4-1-2020 Updates  
  
* Cleaned up code
* Whole table generated not just tbody  
  
$4-22-2020  
  
* Added JSON conversion as a separate file
  
#Usage  
  
* Drop Excel file into the /src folder  
* Run the file path with the Excel file  
* This will create all the tables that you will need in /tables and /tables/fr  
* You'll still need to do some manual work on those html files generated in the /tables  
        * This includes html line breaks  
        * In march.html it starts with undefined text then the rest of the table (until I fix that)


** Inject copy to Regional pages - airTRFX **
```
node airtrfx-routes.js -f "c:\Users\..\ac-covid-19\src\excel-file.xlsx"
```

#Usage  
  
* Drop Excel file into the /src folder  
* Run the the above piece of code with your file path
* Files will be created by date under /airTRFX folder.
* Upload all 3 files to /content/dam/aircanada/airtrfx/data/ and clear cache