const Excel = require("exceljs");
const minimist = require("minimist");
const fs = require("fs");

const {
	sheetsToParse,
} = require("./app/config/app.json");
let c19Routes = [],
    c19RoutesFR = [];
let c19Obj = {},
	c19ObjFR = {};

const args = minimist(process.argv.slice(2));

console.log("sheetsToParse = ", sheetsToParse);

try {
	if (!args.f) {
		throw new Error("Missing File argument");
	}
} catch (err) {
	console.log("err = ", err);
}

const workbook = new Excel.Workbook();
console.log("sheetsToParse = ", sheetsToParse);
const parseDoc = async () => {
	console.log("args = ", args.f);
	const worksheets = await workbook.xlsx.readFile(args.f);
	worksheets.eachSheet((worksheet) => {
		console.log(worksheet.name);
		if (String(worksheet.name) != "COPY" && String(worksheet.name) != "SPECIAL FLIGHTS") {
			worksheet.eachRow({
				includeEmpty: false,
			}, (row, rowNumber) => {
				if (String(row.getCell(1).value) != worksheet.getCell("A1").value && String(row.getCell(2).value) != worksheet.getCell("B1").value) {
					console.log(`Row ${rowNumber} = ${JSON.stringify(row.values)}`);
					c19Obj = { "origin" : "", "destination" : "", "route" : String(row.getCell(1).text), "August" : String(row.getCell(2).text), "September" : String(row.getCell(3).text), "October" : String(row.getCell(4).text),"Comments":String(row.getCell(5).text) };
					c19ObjFR = { "origin" : "", "destination" : "", "route" : String(row.getCell(7).text), "August" : String(row.getCell(8).text), "September" : String(row.getCell(9).text), "October" : String(row.getCell(10).text),"Comments": String(row.getCell(11).text)};
					c19Routes.push(c19Obj);
					c19RoutesFR.push(c19ObjFR);
				}
			});
			var nameToWrite = worksheet.name.trim().toLowerCase().replace(/\s+/g, '-').replace(/-{2,}/g,'-');
			fs.writeFile(`./regions/${nameToWrite}.json`, /*travelTable*/ JSON.stringify(c19Routes), (err) => {
				if (err) throw err;
				console.log(`${nameToWrite} table saved.`);
			});
			fs.writeFile(`./regions/${nameToWrite}-fr.json`, /*travelTableFR*/ JSON.stringify(c19RoutesFR), (err) => {
				if (err) throw err;
				console.log(`FRENCH ${nameToWrite} table saved.`);
			});
			c19Routes = [];
			c19RoutesFR = [];
		}
	});
};

parseDoc();
